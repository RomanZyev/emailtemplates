class CreateEmailtemplates < ActiveRecord::Migration
  def self.up
    create_table :emailtemplates do |t|
      t.belongs_to  :project, index: true
      t.string      :email
      t.string      :subject
      t.text        :body
      t.integer     :issue_statuse_id
    end
  end
end
