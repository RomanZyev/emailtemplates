resources :emailtemplates

resources :projects do
  resources :emailtemplates, :controller => 'emailtemplates',
            :only => [:index, :new, :create, :edit, :update, :destroy]
end 