require_dependency 'mailer'

module Patches
  module MailerPatch
    def self.included(base)
      base.send(:include, InstanceMethods)      
    end
    
   module InstanceMethods
      
     def emailtemplate(journal)
        issue = journal.journalized
        @issue = issue
        @project = issue.project
        @statuse = @issue.status
        @issue_url = url_for(:controller => 'issues', :action => 'show', :id => issue, :anchor => "change-#{journal.id}")
        emil_template = Emailtemplate.where(project_id: @project.id, issue_statuse_id: @statuse.id )
        emil_template.each do |template|
          @body = template.body
          mail :to => template.email,:subject => template.subject
          @statuse.id
        end
      end
    end
  end
end
