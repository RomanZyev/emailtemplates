module Emailtemplates
    class ControllerIssuesEditAfterSaveHook < Redmine::Hook::ViewListener
   
      def controller_issues_edit_after_save(context={})
        journal = context[:journal]
        if journal.new_value_for('status_id') 
           Mailer.emailtemplate(journal).deliver
        end        
      end
    end
end