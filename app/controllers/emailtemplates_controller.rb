class EmailtemplatesController < ApplicationController
  default_search_scope :emailtemplates
  model_object Emailtemplate
  before_filter :find_model_object, :except => [:new, :create, :index]
  before_filter :find_project_from_association, :except => [:new, :create, :index]
  before_filter :find_optional_project
  menu_item :templates

  def index
     @emailtemplates = Emailtemplate.where(project_id: @project.id)
  end

  def new
    @emailtemplate = Emailtemplate.new
    render :action => :edit
  end

  def create
    @emailtemplate = Emailtemplate.create(ad_params)
    if @emailtemplate.save
      redirect_to project_emailtemplates_path(@emailtemplate.project_id)
    else
      render :action => 'edit'
    end

  end

  def edit
    @emailtemplate = Emailtemplate.find(params[:id])
  end

  def update
    @emailtemplates = Emailtemplate.find(params[:id])
    @emailtemplates.update_attributes(ad_params)
    if @emailtemplates.save
      redirect_to project_emailtemplates_path(@project)
    else
      render :action => 'edit'
    end
  end

  def destroy
    @emailtemplates = Emailtemplate.find(params[:id])
    @emailtemplates.destroy
    redirect_to project_emailtemplates_url
  end

  private

  def find_optional_project
    return true unless params[:project_id]
    @project = Project.find(params[:project_id])
  rescue ActiveRecord::RecordNotFound
    render_404
  end
  def ad_params
    params.require(:emailtemplate).permit(:project_id, :email, :subject, :body, :issue_statuse_id)
  end
end
