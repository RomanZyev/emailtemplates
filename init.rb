Redmine::Plugin.register :emailtemplates do
  name 'Email Templates plugin'
  author 'Zyev Roman'
  description 'This is a plugin for Redmine'
  version '0.0.1'
  url ''
  author_url ''

  permission :emailtemplates, { :emailtemplates => [:index] }, :admin => true
  menu :project_menu, :emailtemplates, { :controller => 'emailtemplates', :action => 'index' },
       :caption => 'Email Templates',
       :after => :activity,
       :param => :project_id
   
  settings :default => {:controller => 'correnly', :action => 'index', 'empty' => true}, :partial => 'settings_emailtemplate'
  
end

Rails.application.config.to_prepare do
    Mailer.send(:include, Patches::MailerPatch)   
end

require 'patches/hooks/controller_issues_edit_after_save_hook'